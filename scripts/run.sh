#!/bin/bash
python manage.py makemigrations
python manage.py migrate

python manage.py makemigrations
python manage.py migrate

python manage.py collectstatic --noinput
gunicorn --bind 0:8000 app.wsgi --timeout 120 --log-level=DEBUG --keep-alive 5
