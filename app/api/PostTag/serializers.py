from rest_framework import serializers
from api.models import PostTag


class PostTagSerializer(serializers.ModelSerializer):
    tag = serializers.SlugRelatedField(read_only=True, slug_field='name')

    class Meta:
        model = PostTag
        fields = ['id', 'tag']
        read_only_fields = ['id']
