from django.urls import path
from .views import GetAllCategories


app_name = 'categories'

urlpatterns = [
    path('', GetAllCategories.as_view(), name='all-categories')
]
