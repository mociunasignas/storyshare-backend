from rest_framework.generics import ListAPIView
from api.categories.serializers import CategorySerializer
from api.helpers.pagination import StandardResultsSetPagination
from api.models import Category


class GetAllCategories(ListAPIView):
    pagination_class = StandardResultsSetPagination
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
