from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from api.posts.serializers import NestedPostSerializer
from django.contrib.auth import get_user_model
from api.models import Post
from api.reply.serializers import ReplySerializer

User = get_user_model()


class CreateGetReply(ListCreateAPIView):
    serializer_class = ReplySerializer
    queryset = Post.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = Post.objects.filter(parent=self.kwargs.get("pk"))
        serializer = NestedPostSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data=request.data,
            context={'request': request},
        )
        serializer.is_valid(raise_exception=True)
        post = serializer.create(serializer.validated_data)
        return Response(NestedPostSerializer(post).data)
