from django.urls import path
from .views import CreateGetReply

app_name = 'reply'

urlpatterns = [
    path('<int:pk>/', CreateGetReply.as_view(), name='create-get-reply')
]
