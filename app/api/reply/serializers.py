from django.contrib.auth import get_user_model
from rest_framework import serializers
from api.models import Post, VideoUploadModel

User = get_user_model()


class ReplySerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'title', 'parent', 'created', 'user']
        read_only_fields = ['id', 'created', 'user']

    def create(self, validated_data):
        request = self.context.get("request")
        # new_video = VideoUploadModel.objects.get(video_uuid=validated_data['video_uuid'])
        new_video = VideoUploadModel.objects.get(video_uuid=request.data['video_uuid'])
        parent = self.context['view'].get_object()
        new_title = f'RE:{parent.title}'
        validated_data['title'] = new_title
        category_id = parent.category_id
        return Post.objects.create(
            **validated_data,
            user=self.context.get('request').user,
            parent=parent,
            category_id=category_id,
            video=new_video,
        )
