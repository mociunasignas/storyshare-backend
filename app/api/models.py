from django.contrib.auth import get_user_model
from django.db import models
from api.helpers.codegen import generate_validation_code
from annoying.fields import AutoOneToOneField

# from app.storage_backends import PublicMediaStorage
# from django.conf import settings
# import uuid

User = get_user_model()


class PostStatus:
    draft = 'DRAFT'
    published = 'PUBLISHED'


POST_STATUS_CHOICES = (
    (PostStatus.draft, 'DRAFT'),
    (PostStatus.published, 'PUBLISHED')
)


# AutoOneToOneField can prevents errors when doing a reverse lookup (user.profile)


class UserProfile(models.Model):
    user = AutoOneToOneField(to=User, on_delete=models.CASCADE, related_name='profile')

    avatar = models.ImageField(null=True, blank=True)
    about = models.CharField(max_length=300, blank=True)
    location = models.CharField(null=True, blank=True, max_length=50)
    phone = models.CharField(null=True, blank=True, max_length=15)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    code = models.CharField(max_length=16, default=generate_validation_code)

    def generate_code(self):
        self.code = generate_validation_code()
        self.save()
        return self.code

    def __str__(self):
        return str(self.user)


class Follow(models.Model):
    celebrity = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='follower', null=True, blank=True)
    followed_by = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='followee', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.followed_by} follows {self.celebrity}'


class Category(models.Model):
    category_name = models.CharField(null=True, blank=True, max_length=20)

    def __str__(self):
        return self.category_name


class Tag(models.Model):
    name = models.CharField(null=True, blank=True, max_length=20)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class VideoUploadModel(models.Model):
    file = models.FileField(upload_to='uploads/videos', default=None, null=True, blank=True)
    thumbnail = models.FileField(upload_to='uploads/thumbnails', default=None, null=True, blank=True)
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='videos')
    video_url = models.TextField(max_length=200)
    video_uuid = models.TextField(max_length=200)

    def __str__(self):
        return self.video_uuid


class Post(models.Model):
    category = models.ForeignKey(to=Category, related_name='post_category', on_delete=models.CASCADE)
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='posts')
    video = models.TextField(max_length=200)
    parent = models.ForeignKey(to="self", on_delete=models.CASCADE, related_name='replies', default=None, null=True, blank=True)
    title = models.TextField(max_length=60)
    status = models.TextField(max_length=60, choices=POST_STATUS_CHOICES, default=PostStatus.draft)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Like(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='liked_posts', null=True, blank=True)
    post = models.ForeignKey(to=Post, on_delete=models.CASCADE, related_name='likes', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.user} liked {self.post}'


class PostTag(models.Model):
    post = models.ForeignKey(to=Post, on_delete=models.CASCADE, related_name='tags', null=True, blank=True)
    tag = models.ForeignKey(to=Tag, on_delete=models.CASCADE, related_name='posttags', null=True, blank=True)

    def __str__(self):
        return f'Post with title: "{self.post}" added "{self.tag}" as Tag'


class ReportedPost(models.Model):
    post = models.ForeignKey(to=Post, on_delete=models.CASCADE, related_name='reports', null=True, blank=True)
    reason = models.CharField(null=True, blank=True, max_length=300)

    def __str__(self):
        return f'Post: "{self.post}" reported because: "{self.reason}"'
