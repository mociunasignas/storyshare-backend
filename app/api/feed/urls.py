from django.urls import path
from .views import GetOrSearchAllPosts, GetPostsOfSpecificUser, GetPostsOfFollowedUsers


app_name = 'feed'

urlpatterns = [
    path('', GetOrSearchAllPosts.as_view(), name='get-all-posts'),
    path('<int:pk>/', GetPostsOfSpecificUser.as_view(), name='get-posts-of-specific-user'),
    path('followees/', GetPostsOfFollowedUsers.as_view(), name='get-followee-posts'),
]
