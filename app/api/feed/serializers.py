from rest_framework import serializers
from api.models import Post

from django.contrib.auth import get_user_model
User = get_user_model()


class FeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'title', 'status', 'type', 'published', 'parent', 'created', 'updated']
        read_only_fields = ['id', 'title', 'status', 'type', 'published', 'parent', 'created', 'updated']
