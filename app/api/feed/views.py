from api.posts.serializers import NestedPostSerializer
from api.base import GetObjectMixin
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import filters
from api.helpers.pagination import StandardResultsSetPagination

from api.models import Post, Follow
from django.contrib.auth import get_user_model

User = get_user_model()


class GetOrSearchAllPosts(ListAPIView):
    pagination_class = StandardResultsSetPagination
    serializer_class = NestedPostSerializer
    queryset = Post.objects.filter(parent=None).order_by('-id')

    filter_backends = (filters.SearchFilter,)
    search_fields = ['title',
                     'user__username',
                     'user__first_name',
                     'user__last_name',
                     'user__email',
                     'category__category_name',
                     'tags__tag__name',
                     ]


class GetPostsOfSpecificUser(GetObjectMixin, ListAPIView):
    pagination_class = StandardResultsSetPagination
    serializer_class = NestedPostSerializer
    queryset = Post.objects.all().order_by('-id')

    def get_queryset(self, **kwargs):
        return self.queryset.filter(user=kwargs.get('user'))

    def list(self, request, *args, **kwargs):
        user = self.get_object_by_model(User, pk=kwargs.get('pk'))
        queryset = self.get_queryset(user=user)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class GetPostsOfFollowedUsers(GetObjectMixin, ListAPIView):
    pagination_class = StandardResultsSetPagination
    serializer_class = NestedPostSerializer
    queryset = Post.objects.all().order_by('-id')

    def filter_queryset(self, queryset):
        follows = list(Follow.objects.filter(followed_by=self.request.user.id))
        celebrities = []
        for follow in follows:
            celebrities += [follow.celebrity]
        posts = []
        for celebrity in celebrities:
            posts = posts + list(celebrity.posts.all())

        return posts
