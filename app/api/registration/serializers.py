from django.contrib.auth import get_user_model
from rest_framework import serializers
from api.helpers.my_mailer import Mailer

User = get_user_model()


class RegistrationSerializer(serializers.Serializer):
    email = serializers.EmailField(label='Registration E-Mail Address')

    def validate_email(self, email):
        try:
            User.objects.get(email=email)
            raise serializers.ValidationError('User already exist!')
        except User.DoesNotExist:
            return email

    def save(self, validated_data):
        email = validated_data.get('email')
        new_user = User.objects.create_user(username=email, email=email, is_active=False)
        Mailer.send_registration_validation_code(email, new_user.profile.code)
        return new_user


class RegistrationValidationSerializer(RegistrationSerializer):
    code = serializers.CharField(label='Validation code', write_only=True)
    password = serializers.CharField(label='password', write_only=True)
    first_name = serializers.CharField(label='First name')
    last_name = serializers.CharField(label='Last name')

    def validate_email(self, email):
        try:
            return User.objects.get(email=email)
        except User.DoesNotExist:
            raise serializers.ValidationError('User does not exist!')

    def validate(self, data):
        user = data.get('email')
        if user.is_active:
            raise serializers.ValidationError({'code': 'already validated'})
        if data.get('code') != user.profile.code:
            raise serializers.ValidationError({'code': 'Wrong validation code'})
        return data

    def save(self, validated_data):
        user = validated_data.get('email')
        user.first_name = validated_data.get('first_name')
        user.last_name = validated_data.get('last_name')
        user.is_active = True
        user.set_password(validated_data.get('password'))
        user.save()
        return user
