from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from api.helpers.my_mailer import Mailer
from .serializers import PasswordResetSerializer, PasswordResetValidationSerializer


class PasswordResetView(GenericAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = PasswordResetSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data.get('email')
        user.profile.generate_code()
        Mailer.send_password_reset_mail(user.email, user.profile.code)
        return Response(f'Password reset validation code sent to email: {user.email}')


class PasswordResetValidationView(GenericAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = PasswordResetValidationSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(serializer.validated_data)
        return Response('Password was updated successfully')
