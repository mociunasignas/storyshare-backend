from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField(label='Registration E-Mail Address')

    def validate_email(self, email):
        try:
            return User.objects.get(email=email)
        except User.DoesNotExist:
            raise serializers.ValidationError('email not found')


# Note by Andi: not checking password repeat here. Can be done in the frontend
# (compare state of both password input fields and dont allow user to send form if not matching)
class PasswordResetValidationSerializer(PasswordResetSerializer):
    code = serializers.CharField(
        label='Validation code',
        write_only=True,
    )
    password = serializers.CharField(
        label='password',
        write_only=True,
    )

    def validate(self, data):
        user = data.get('email')
        if data.get('code') != user.profile.code:
            raise serializers.ValidationError({
                'code': 'invalid code'
            })
        return data

    def save(self, validated_data):
        user = validated_data.get('email')
        user.set_password(validated_data.get('password'))
        user.save()
        user.profile.code = ''
        user.profile.save()
        return user
