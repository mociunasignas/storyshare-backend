from django.contrib.auth import get_user_model
from rest_framework import serializers
from api.models import UserProfile
from drf_writable_nested import WritableNestedModelSerializer

User = get_user_model()


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ['avatar', 'about', 'location', 'phone', 'created', 'updated']


class NestedUserSerializer(WritableNestedModelSerializer):
    post_count = serializers.SerializerMethodField()
    fame_index = serializers.SerializerMethodField()
    follower_count = serializers.SerializerMethodField()
    followee_count = serializers.SerializerMethodField()

    profile = UserProfileSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'profile', 'post_count', 'fame_index', 'follower_count', 'followee_count']

    def get_post_count(self, user):
        return user.posts.count()

    def get_follower_count(self, user):
        return user.follower.count()

    def get_followee_count(self, user):
        return user.followee.count()

    def get_fame_index(self, user):
        return sum([p.likes.count() for p in user.posts.all()])
