from django.contrib.auth import get_user_model
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.models import UserProfile
from .serializers import NestedUserSerializer
from api.permissions import IsOwnerOrReadOnly

User = get_user_model()


class GetUpdateUserProfile(RetrieveUpdateAPIView):
    serializer_class = NestedUserSerializer
    permission_classes = [
        IsAuthenticated,
        IsOwnerOrReadOnly,
    ]
    queryset = UserProfile.objects.all()

    def retrieve(self, request, *args, **kwargs):
        instance = User.objects.get(id=self.request.user.id)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def patch(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = User.objects.get(id=self.request.user.id)
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        instance._prefetched_objects_cache = {}
        return Response(serializer.data)
