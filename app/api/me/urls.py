from django.urls import path
from .views import GetUpdateUserProfile

app_name = 'me'

urlpatterns = [
    path('', GetUpdateUserProfile.as_view(), name='get-update-user-profile'),
]
