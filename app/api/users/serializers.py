from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    post_count = serializers.SerializerMethodField()
    fame_index = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username', 'post_count', 'fame_index']
        read_only_fields = fields

    def get_post_count(self, user):
        return user.posts.count()

    def get_fame_index(self, user):
        return sum([p.likes.count() for p in user.posts.all()])
