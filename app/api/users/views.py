from django.db.models import Q
from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView, DestroyAPIView
from rest_framework.response import Response

from api.helpers.pagination import StandardResultsSetPagination
from api.me.serializers import NestedUserSerializer

from api.models import Follow
from django.contrib.auth import get_user_model

User = get_user_model()


class GetSearchUsers(ListAPIView):
    pagination_class = StandardResultsSetPagination

    serializer_class = NestedUserSerializer
    queryset = User.objects.filter(is_active=True)

    def filter_queryset(self, queryset):
        search_string = self.request.query_params.get('search')

        if search_string:
            queryset = queryset.filter(
                Q(username__contains=search_string) |
                Q(email__contains=search_string) |
                Q(first_name__contains=search_string) |
                Q(last_name__contains=search_string)
            )
        return queryset


class FollowUnfollowUser(CreateAPIView, DestroyAPIView):

    def create(self, request, *args, **kwargs):
        follower = request.user
        celebrity = User.objects.get(id=kwargs['pk'])
        new_follow, created = Follow.objects.get_or_create(celebrity=celebrity, followed_by=follower)
        if created:
            return Response('Following added.', status=status.HTTP_201_CREATED)
        else:
            return Response('Following already exists.', status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        follower = request.user
        celebrity = User.objects.get(id=kwargs['pk'])
        try:
            follow = Follow.objects.get(celebrity=celebrity, followed_by=follower)
        except:
            return Response('You are not following this user', status=status.HTTP_200_OK)
        follow.delete()
        return Response('Following removed.', status=status.HTTP_204_NO_CONTENT)


class FollowerList(ListAPIView):
    pagination_class = StandardResultsSetPagination
    serializer_class = NestedUserSerializer
    queryset = User.objects.all()

    def filter_queryset(self, queryset):
        return queryset.filter(followee__celebrity=self.request.user)


class FollowingList(FollowerList):
    def filter_queryset(self, queryset):
        return queryset.filter(follower__followed_by=self.request.user)


class GetSpecificUser(RetrieveAPIView):
    serializer_class = NestedUserSerializer
    queryset = User.objects.all()
