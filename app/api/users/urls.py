from django.urls import path
from .views import GetSearchUsers, FollowUnfollowUser, FollowerList, FollowingList, GetSpecificUser

app_name = 'users'

urlpatterns = [
    path('', GetSearchUsers.as_view(), name='get-search-user'),
    path('follow/<int:pk>/', FollowUnfollowUser.as_view(), name='follow-unfollow-user'),
    path('followers/', FollowerList.as_view(), name='get-followers'),
    path('following/', FollowingList.as_view(), name='get-following'),
    path('<int:pk>/', GetSpecificUser.as_view(), name='get-specific-user'),
]
