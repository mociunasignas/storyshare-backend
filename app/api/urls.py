from django.urls import path, include


app_name = 'api'

urlpatterns = [
    path('auth/', include('api.auth.urls', namespace='auth')),
    path('registration/', include('api.registration.urls', namespace='registration')),
    path('posts/', include('api.posts.urls', namespace='post')),
    path('users/', include('api.users.urls', namespace='users')),
    path('feed/', include('api.feed.urls', namespace='feed')),
    path('categories/', include('api.categories.urls', namespace='categories')),
    path('me/', include('api.me.urls', namespace='me')),
    path('replies/', include('api.reply.urls', namespace='replies')),
    path('reporting/', include('api.reporting.urls', namespace='reporting')),
    path('upload/', include('api.upload.urls', namespace='upload')),
]
