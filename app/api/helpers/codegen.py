from django.utils.crypto import get_random_string


# TODO encode a python dict in base64 with a expiration epoch timestamp and the code {"code": 12345, "exp": 1563141243}
def generate_validation_code(length=16):
    return get_random_string(length=length)
