from django.contrib.auth import get_user_model
from post_office import mail


User = get_user_model()
sender_email = 'students@propulsionacademy.com'


# whenever you have to send a mail somewhere create a method inside this class
# to send the actual mail just import Mailer and call the method
# content of the mail can come from a template (created in django admin), but lets not use it at this point
# (templates are stored on the db so its a pain if it gets wiped)
# for now set priority='now' to send mails immediately.
# have a look at https://github.com/ui/django-post_office for more details
# TODO setup cron on the droplet that executes "python manage.py send_queued_mail" every minute (low/medium)
# TODO check user notify preferences to see if they want to receive emails (low)
class Mailer:
    @staticmethod
    def send_registration_validation_code(email, code):
        mail.send(
            email,
            sender_email,
            subject='My email',
            message='Hi there!',
            html_message=f'<h1>Your registration validation code: {code}</h1>',
            priority='now'
        )

    @staticmethod
    def send_password_reset_mail(email, code):
        mail.send(
            email,
            sender_email,
            subject='My email',
            message='Hi there!',
            html_message=f'<h1>Your password reset validation code: {code}</h1>',
            priority='now'
        )
