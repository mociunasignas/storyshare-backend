from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from .models import Post, Follow, Category, UserProfile, Like, Tag, PostTag, ReportedPost, VideoUploadModel

User = get_user_model()


# https://stackoverflow.com/questions/3400641/how-do-i-inline-edit-a-django-user-profile-in-the-admin-interface
class UserProfileInline(admin.StackedInline):
    model = UserProfile
    max_num = 1
    can_delete = False


class UserAdmin(AuthUserAdmin):
    def add_view(self, *args, **kwargs):
        self.inlines = []
        return super(UserAdmin, self).add_view(*args, **kwargs)

    def change_view(self, *args, **kwargs):
        self.inlines = [UserProfileInline]
        return super(UserAdmin, self).change_view(*args, **kwargs)


# unregister old user admin
admin.site.unregister(User)

# register new user admin
admin.site.register(User, UserAdmin)
admin.site.register(Post)
admin.site.register(Follow)
admin.site.register(Category)
admin.site.register(UserProfile)
admin.site.register(Like)
admin.site.register(Tag)
admin.site.register(PostTag)
admin.site.register(ReportedPost)
admin.site.register(VideoUploadModel)
