from rest_framework import status, mixins
from rest_framework.generics import GenericAPIView, ListAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from api.base import GetObjectMixin
from api.helpers.pagination import StandardResultsSetPagination
from .serializers import PostSerializer, NestedPostSerializer
from api.models import Post, Tag, Category, PostTag, VideoUploadModel
from api.models import Like

from api.permissions import IsOwnerOrReadOnly


class PostCreate(CreateAPIView):
    queryset = Post.objects.all()
    serializer_class = NestedPostSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def create(self, request, **kwargs):
        # create new tags in Tags table
        tags_to_check = self.request.data["tags"]
        for i in tags_to_check:
            Tag.objects.get_or_create(name=i)

        # create new post in Posts table
        title = request.data['title']
        category_id = request.data['category_id']
        user = request.user
        new_category = Category.objects.get(id=category_id)
        new_video = VideoUploadModel.objects.get(video_uuid=request.data['video_uuid'])
        print(new_category)
        new_post = Post.objects.create(
            title=title,
            category=new_category,
            user=user,
            video=new_video,
        )

        # create new post-tag in PostTag table
        for i in set(tags_to_check):
            PostTag.objects.create(
                tag=Tag.objects.get(name=i),
                post=new_post
            )

        return Response(self.get_serializer(new_post).data)


class GetUpdateDeletePost(RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = NestedPostSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def update(self, request, *args, **kwargs):

        # create new tags in Tags table
        tags_to_check = self.request.data["tags"]
        for i in tags_to_check:
            Tag.objects.get_or_create(name=i)

        # create new post in Posts table
        partial = kwargs.pop('partial', False)
        instance = self.get_object()

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        tags_in_posttag = PostTag.objects.filter(post=self.get_object())
        tags_names = [posttag.tag.name for posttag in tags_in_posttag]

        tag_delta = set(tags_to_check).symmetric_difference(set(tags_names))
        print(tag_delta)
        for tag in tag_delta:
            if tag in tags_to_check:
                # new post-tag relationship to add
                PostTag.objects.create(post=self.get_object(), tag=Tag.objects.get(name=tag))
            else:
                # old post-tag relationship to remove
                post_tag = PostTag.objects.get(post=self.get_object(), tag=Tag.objects.get(name=tag))
                post_tag.delete()

                # todo check if we deleted the last instance of tag
                # db_tag = Tag.objects.get(name=Tag.objects.get(name=tag))

        return Response(serializer.data)


class LikeDislikePost(GetObjectMixin, GenericAPIView, mixins.DestroyModelMixin):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def post(self, request, **kwargs):
        user = request.user
        try:
            post = Post.objects.get(id=kwargs['pk'])
        except:
            return Response('Post doesn\'t exist.', status=status.HTTP_404_NOT_FOUND)

        new_like, created = Like.objects.get_or_create(user=user, post=post)
        if created:
            new_like.save()
            new_post = Post.objects.get(id=kwargs['pk'])
            return Response(self.get_serializer(new_post).data, status=status.HTTP_201_CREATED)
        else:
            return Response('Like already exists.', status=status.HTTP_200_OK)

    def delete(self, request, **kwargs):
        user = request.user
        try:
            post = Post.objects.get(id=kwargs['pk'])
        except:
            return Response('Post doesn\'t exist.', status=status.HTTP_404_NOT_FOUND)

        try:
            like = Like.objects.get(user=user, post=post)
        except:
            return Response('Like not found', status=status.HTTP_404_NOT_FOUND)
        self.perform_destroy(like)
        return Response('Like removed', status=status.HTTP_204_NO_CONTENT)


class LikedPostsView(ListAPIView):
    pagination_class = StandardResultsSetPagination
    serializer_class = PostSerializer

    def get_queryset(self):
        return Post.objects.filter(likes__user=self.request.user).order_by('-id')
