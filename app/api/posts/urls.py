from django.urls import path
from .views import PostCreate, GetUpdateDeletePost, LikedPostsView, LikeDislikePost


app_name = 'posts'

urlpatterns = [
    path('', PostCreate.as_view(), name='post-get-update-delete'),
    path('<int:pk>/', GetUpdateDeletePost.as_view(), name='post-get-update-delete'),
    path('like/<int:pk>/', LikeDislikePost.as_view(), name='like-dislike-posts'),
    path('liked/', LikedPostsView.as_view(), name='get-liked-posts'),
]
