from drf_writable_nested import WritableNestedModelSerializer
from api.PostTag.serializers import PostTagSerializer
from api.models import Post
from django.contrib.auth import get_user_model
from rest_framework import serializers
from api.users.serializers import UserSerializer

User = get_user_model()


class PostSerializer(serializers.ModelSerializer):
    like_count = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Post
        read_only_fields = ['id', 'created', 'updated']
        fields = '__all__'
        depth = 1

    def create(self, validated_data):
        return Post.objects.create(
            **validated_data,
            user=self.context.get('request').user,
        )

    def get_like_count(self, post):
        return post.likes.count()


class NestedPostSerializer(WritableNestedModelSerializer):
    user = UserSerializer(read_only=True)
    # video = serializers.SlugRelatedField(read_only=True, slug_field='video_uuid')
    tags = PostTagSerializer(read_only=True, many=True)
    category = serializers.SlugRelatedField(read_only=True, many=False, slug_field='category_name')

    like_count = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Post
        # fields = ['id', 'title', 'status', 'created', 'updated', 'parent', 'user', 'tags', 'category' 'like_count']
        fields = '__all__'

    def get_like_count(self, post):
        return post.likes.count()
