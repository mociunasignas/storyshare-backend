from django.urls import path
from .views import UploaderView

app_name = 'upload'

urlpatterns = [
    path('', UploaderView.as_view(), name='upload'),
]
