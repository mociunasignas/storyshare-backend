from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.status import HTTP_200_OK
from api.permissions import IsOwnerOrReadOnly
from .serializers import UploaderSerializer
from api.models import VideoUploadModel
from rest_framework.response import Response


class UploaderView(CreateAPIView):
    queryset = VideoUploadModel.objects.all()
    serializer_class = UploaderSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def get(self, request, *args, **kwargs):
        print(request)
        posts = VideoUploadModel.objects.all()
        serializer = UploaderSerializer(posts, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs, ):
        uuid = self.request.data['video_uuid']
        cdn_url = 'https://cdn.strav.us/media/uploads/videos/'
        user = request.user
        url = f"{cdn_url}{uuid}"
        thumbnail = self.request.FILES['thumbnail']
        file = self.request.FILES['file']
        new_video = VideoUploadModel.objects.create(
            user=user,
            video_url=url,
            video_uuid=uuid,
            thumbnail=thumbnail,
            file=file
        )

        return Response(self.get_serializer(new_video).data, status=HTTP_200_OK)
