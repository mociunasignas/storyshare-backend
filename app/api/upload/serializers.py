from rest_framework import serializers
from api.models import VideoUploadModel


class UploaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoUploadModel
        fields = '__all__'
