from rest_framework import serializers
from api.models import VideoUploadModel


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoUploadModel
        fields = ['id', 'file', 'user', 'video_url', 'image']
        read_only_fields = ['id', 'user']
