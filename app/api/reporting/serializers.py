from rest_framework import serializers
from api.models import ReportedPost


class ReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportedPost
        fields = ['id', 'post', 'reason']
        read_only_fields = ['id']
