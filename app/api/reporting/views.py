from rest_framework.generics import ListAPIView

from api.helpers.pagination import StandardResultsSetPagination
from api.reporting.serializers import ReportSerializer
from api.models import ReportedPost


class GetReportedPosts(ListAPIView):
    pagination_class = StandardResultsSetPagination
    serializer_class = ReportSerializer
    queryset = ReportedPost.objects.all().order_by('-id')
