from django.urls import path
from .views import GetReportedPosts

app_name = 'reporting'

urlpatterns = [
    path('', GetReportedPosts.as_view(), name='get-reported-posts')
]
