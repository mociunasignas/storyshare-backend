#FROM frolvlad/alpine-miniconda3
#RUN apk add --no-cache shadow bash
FROM continuumio/miniconda:4.4.10

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN apt-get update && apt-get install -qqy \
    wget \
    bzip2 \
    libssl-dev \
    openssh-server

RUN apt-get autoremove -y

# SSH Server
RUN mkdir /var/run/sshd
RUN echo 'root:screencast' | chpasswd
RUN sed -i '/PermitRootLogin/c\PermitRootLogin yes' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

## Start: SSH
#RUN apk add --no-cache openssh openrc
#RUN rc-update add sshd
#RUN mkdir /var/run/sshd
#
#RUN echo 'root:screencast' | chpasswd
#RUN sed -i '/PermitRootLogin/c\PermitRootLogin yes' /etc/ssh/sshd_config
#
#ENV NOTVISIBLE "in users profile"
#RUN echo "export VISIBLE=now" >> /etc/profile
## End: SSH

RUN mkdir -p /app && \
    mkdir -p /backend && \
    mkdir -p /scripts && \
    ls /etc && \
    ls /root && \
    touch /root/.profile

COPY ./requirements.yml ./backend/requirements.yml

RUN /opt/conda/bin/conda env create -f ./backend/requirements.yml
ENV PATH /opt/conda/envs/backend/bin:$PATH

RUN echo "source activate backend" >~/.bashrc

COPY ./app /backend
COPY ./scripts /scripts
RUN chmod +x /scripts/*

WORKDIR /backend

EXPOSE 8000
EXPOSE 22
